In the file `main.py` you can find code to get abstract about some topic.

Your task is to refactor it and add some tests (1 is enough).

Online shell:
(repl.it)[https://repl.it/@AntonPererva/TestTaskRefactor#readme.md]


To run tests in online shell:
- Press run to update code
- Paste lines below to the shell
  ```
  import pytest
  pytest.main()
  ```
