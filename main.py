import requests


def findAbstract(topic):
    q = 'wiki ' + topic
    url = 'https://api.duckduckgo.com/?q=' + q + '&format=json'
    response = requests.get(url)
    data = response.json()
    abstract = data['Abstract']
    if abstract == '':
        raise ValueError('error')
    return abstract


print(findAbstract('Elon Tusk'))
